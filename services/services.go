package services

import (
	"api_gateway/config"
	"api_gateway/genproto/crm_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	CrmService() crm_service.SuperAdminServiceClient         //
	Branch() crm_service.BranchServiceClient                 //
	Manager() crm_service.ManagerServiceClient               //
	Teacher() crm_service.TeacherServiceClient               //
	SupportTeacher() crm_service.SupportTeacherServiceClient //
	Administrator() crm_service.AdministratorServiceClient   //
	Student() crm_service.StudentServiceClient               //
	Group() crm_service.GroupServiceClient                   //
	Event() crm_service.EventServiceClient                   //
	AssignStudent() crm_service.AssignStudentServiceClient   //
	DoTask() crm_service.DoTaskServiceClient                 //
	Score() crm_service.ScoreServiceClient                   //
	Jurnal() crm_service.JurnalServiceClient                 //
	Task() crm_service.TaskServiceClient                     //
	Schedule() crm_service.ScheduleServiceClient             //
	Lesson() crm_service.LessonServiceClient                 //
	Payment() crm_service.PaymentServiceClient               //

}

type grpcClients struct {
	crmService     crm_service.SuperAdminServiceClient
	branch         crm_service.BranchServiceClient
	manager        crm_service.ManagerServiceClient
	teacher        crm_service.TeacherServiceClient
	supportteacher crm_service.SupportTeacherServiceClient
	adminstrator   crm_service.AdministratorServiceClient
	student        crm_service.StudentServiceClient
	group          crm_service.GroupServiceClient
	event          crm_service.EventServiceClient
	assignstudent  crm_service.AssignStudentServiceClient
	dotask         crm_service.DoTaskServiceClient
	score          crm_service.ScoreServiceClient
	jurnal         crm_service.JurnalServiceClient
	task           crm_service.TaskServiceClient
	schedule       crm_service.ScheduleServiceClient
	lesson         crm_service.LessonServiceClient
	payment        crm_service.PaymentServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// User Service...
	connCrmService, err := grpc.Dial(
		cfg.CrmServiceHost+cfg.CrmGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	return &grpcClients{
		crmService:     crm_service.NewSuperAdminServiceClient(connCrmService),
		branch:         crm_service.NewBranchServiceClient(connCrmService),
		manager:        crm_service.NewManagerServiceClient(connCrmService),
		teacher:        crm_service.NewTeacherServiceClient(connCrmService),
		supportteacher: crm_service.NewSupportTeacherServiceClient(connCrmService),
		adminstrator:   crm_service.NewAdministratorServiceClient(connCrmService),
		student:        crm_service.NewStudentServiceClient(connCrmService),
		group:          crm_service.NewGroupServiceClient(connCrmService),
		event:          crm_service.NewEventServiceClient(connCrmService),
		assignstudent:  crm_service.NewAssignStudentServiceClient(connCrmService),
		dotask:         crm_service.NewDoTaskServiceClient(connCrmService),
		score:          crm_service.NewScoreServiceClient(connCrmService),
		jurnal:         crm_service.NewJurnalServiceClient(connCrmService),
		task:           crm_service.NewTaskServiceClient(connCrmService),
		schedule:       crm_service.NewScheduleServiceClient(connCrmService),
		lesson:         crm_service.NewLessonServiceClient(connCrmService),
		payment:        crm_service.NewPaymentServiceClient(connCrmService),
	}, nil
}

func (g *grpcClients) CrmService() crm_service.SuperAdminServiceClient {
	return g.crmService
}
func (g *grpcClients) Branch() crm_service.BranchServiceClient {
	return g.branch
}
func (g *grpcClients) Manager() crm_service.ManagerServiceClient {
	return g.manager
}
func (g *grpcClients) Teacher() crm_service.TeacherServiceClient {
	return g.teacher
}
func (g *grpcClients) SupportTeacher() crm_service.SupportTeacherServiceClient {
	return g.supportteacher
}
func (g *grpcClients) Administrator() crm_service.AdministratorServiceClient {
	return g.adminstrator
}

func (g *grpcClients) Group() crm_service.GroupServiceClient {
	return g.group
}
func (g *grpcClients) Student() crm_service.StudentServiceClient {
	return g.student
}
func (g *grpcClients) Event() crm_service.EventServiceClient {
	return g.event
}
func (g *grpcClients) AssignStudent() crm_service.AssignStudentServiceClient {
	return g.assignstudent
}
func (g *grpcClients) DoTask() crm_service.DoTaskServiceClient {
	return g.dotask
}
func (g *grpcClients) Score() crm_service.ScoreServiceClient {
	return g.score
}
func (g *grpcClients) Jurnal() crm_service.JurnalServiceClient {
	return g.jurnal
}
func (g *grpcClients) Task() crm_service.TaskServiceClient {
	return g.task
}
func (g *grpcClients) Schedule() crm_service.ScheduleServiceClient {
	return g.schedule
}
func (g *grpcClients) Lesson() crm_service.LessonServiceClient {
	return g.lesson
}
func (g *grpcClients) Payment() crm_service.PaymentServiceClient {
	return g.payment
}

// func (g *grpcClients) ProviderService() user_service.ProviderServiceClient {
// 	return g.provider
// }
